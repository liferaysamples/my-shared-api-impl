package space.manhart.demo.shared.impl;

import org.osgi.service.component.annotations.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import space.manhart.demo.shared.api.MySharedServiceProvider;

// COMMENT: changes start
/**
 * This class implements the public api interface
 * {@link MySharedServiceProvider}.<br/>
 * For Liferay to automatically bind the implementation to the interface, we
 * need to define the interface as service in the @Component annotation.
 * 
 * @author Manuel Manhart
 * @see MySharedServiceProvider
 */
// @formatter:off
@Component(
    immediate = true,
    property = {
    },
    service = MySharedServiceProvider.class
)
// @formatter:on
public class MySharedServiceProviderImpl implements MySharedServiceProvider {
	Logger log = LoggerFactory.getLogger(this.getClass());

	@Override
	public String getGreeting(String name) {
		return "Hello " + name + "!";
	}

	@Override
	public void greet(String name) {
		log.info(getGreeting(name));
		System.out.println(getGreeting(name));
	}

}
// COMMENT: changes end
